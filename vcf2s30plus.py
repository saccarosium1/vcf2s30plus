"""
Fields:
- N:LastName;FirstName;MiddleName;HonorificPrefix;HonorificSuffixes
"""


class Contact:
    def __init__(self):
        self.VERSION = "VERSION:2.1"
        self.NAME = ""
        self.LASTNAME = ""
        self.MIDDLENAME = ""
        self.HPREFIX = ""
        self.HSUFFIXES = ""
        self.BDAY = None
        self.PHONES = []

    def is_valid(self):
        return self.LASTNAME != "" and self.PHONES

    def set_n_field(self, raw_str):
        # Remove 'N:' and split for ';'
        n_field = raw_str.split(":")[1].split(";")
        self.LASTNAME = n_field[0]
        self.NAME = n_field[1]
        self.MIDDLENAME = n_field[2]
        self.HPREFIX = n_field[3]
        self.HSUFFIXES = n_field[4]

    def get_n_field(self, variant):
        if variant is not None:
            self.NAME += f" {variant}"
        res = ""
        if self.NAME != "":
            res += self.NAME + " "
        if self.MIDDLENAME != "":
            res += self.MIDDLENAME + " "
        if self.LASTNAME != "":
            res += self.LASTNAME
        return res.strip()

    def __str__(self):
        res = ""
        for i, number in enumerate(self.PHONES):
            res += "BEGIN:VCARD"
            res += "\n"
            res += "VERSION:2.1"
            res += "\n"
            name = self.get_n_field(i if i > 0 else None)
            res += "N:" + name
            res += "\n"
            res += "FN:" + name
            res += "\n"
            res += f"TEL;CELL:{number}"
            res += "\n"
            res += "END:VCARD"
            res += "\n"
        return res


sou = map(str.strip, open("./contacts.vcf").read().splitlines())
ignored = 0
contacts = []

for s in sou:
    if s == "BEGIN:VCARD":
        contact = Contact()

    elif s.startswith("N:"):
        contact.set_n_field(s)

    elif s.startswith("TEL"):
        contact.PHONES.append(s.split(":", 1)[1].strip().replace(" ", ""))

    elif s.startswith("BDAY"):
        contact.BDAY = s.split(":", 1)[1]

    elif s == "END:VCARD":
        if not contact.is_valid():
            ignored += 1
        else:
            contacts.append(contact)


output = open("./output.vcf", "w")
for x in contacts:
    output.write(str(x))
    output.write("\n")
output.close()

print("#### DONE ####")
if ignored:
    print(f"{ignored} ignored contact(s)")
