#!/bin/sh

set -eu

python3 ./vcf2s30plus.py && nvim --headless -u NONE -c ":set ff=dos | w | q" output.vcf
